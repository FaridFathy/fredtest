package com.thomas.chatsocket;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

public class MainActivity extends AppCompatActivity
{
   ChatApplication app ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        app = (ChatApplication) getApplication();
        initSocket();

    }

    private void initSocket()
    {
        app.getSocket().on(Socket.EVENT_CONNECT, new Emitter.Listener()
        {
            @Override
            public void call(Object... args)
            {
                Log.d("Test", "connected");
//                app.getSocket().emit("auth.authenticate", "[token :" + Constants.TOKEN + "]");
                app.getSocket().emit("auth.authenticate", "[\"token\":\"" + Constants.TOKEN + "\"]");
            }
        });

        app.getSocket().on("auth.authenticated", new Emitter.Listener()
        {
            @Override
            public void call(Object... args)
            {
                Log.d("Test", String.valueOf(args[0]));
            }
        });

        app.getSocket().connect();
    }


//    private Emitter.Listener onTransport = new Emitter.Listener()
//    {
//        @Override
//        public void call(Object... args)
//        {
//
//            Transport transport = (Transport) args[0];
//            transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener()
//            {
//                @Override
//                public void call(Object... args)
//                {
//                    @SuppressWarnings("unchecked") Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
//
//                    String bearer = "bearer " + token;
//                    headers.put("token", Arrays.asList(bearer));
//
//
//                }
//            }).on("auth.authenticate", new Emitter.Listener()
//            {
//                @Override
//                public void call(Object... args)
//                {
//                    @SuppressWarnings("unchecked") Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
//                    // access response headers
//                    String cookie = headers.get("Set-Cookie").get(0);
//
//                    Log.d("res", String.valueOf(args.length));
//                }
//            });
//        }
//    };


}





